import { Injectable } from '@angular/core';
import * as tf from '@tensorflow/tfjs';
import * as mobilenetModule from '@tensorflow-models/mobilenet';
import { LabeledDataManagerService } from '../services/labeled-data-manager.service';
import { Observable, from, of } from 'rxjs';
import { MLState, Label } from '../interfaces/interfaces';

const IMAGE_SIZE = 227;

@Injectable({
  providedIn: 'root'
})
export class ImageClassifierService {
  ready = false;
  mobilenet = null;
  targetModel = null;
  labels = [];
  dataset = {
    data: [],
    labels: [],
    dataArray: []
  };

  constructor(private labeledDataService: LabeledDataManagerService) {

    // Si desde Scratch se produce un nuevo modelo, hay que cargarlo en learningML
   
    let ch = new BroadcastChannel("new-model-from-scratch");
    ch.addEventListener('message', () => {
      console.log("recargando el modelo en LML");
      this.buildDataset()
      tf.loadLayersModel('indexeddb://image-model')
        .then(tm => {
          this.targetModel = tm;
          return tm;
        })
    })

    this.loadMobileNet();
  }

  clear() {
    this.labels = [];
    this.dataset = {
      data: [],
      labels: [],
      // Contains the same info that data but in array form.
      // This is needed to serialize when saving in localstorage
      dataArray: []
    };
    tf.disposeVariables();
  }

  async loadMobileNet() {
    /** 
    * Se carga el modelo de clasificación de imágenes Mobilenet
    * https://arxiv.org/abs/1704.04861
    */
    this.mobilenet = await mobilenetModule.load();
    this.ready = true;
  }

  buildModel() {
    /**
    * Creamos un modelo consistente en una red neuronal con una capa de entrada,
    * una capa oculta y una capa de salida. 
    * 
    * La capa de entrada recibe un tensor de forma (shape) [1024], es decir que hay 
    * 1024 entradas, estas se corresponden con la salida del modelo mobilenet truncada
    * hasta la capa 'conv_preds'. De manera que usamos ese modelo truncado para transformar
    * la imagen en un tensor de shape [1, 1024]. Y es ese tensor el que usamos como entrada
    * de nuestro modelo, es decir, del modelo que se especifica aquí. 
    */
    if (this.targetModel != null) this.targetModel.dispose();

    let that = this;
    this.targetModel = tf.sequential({
      layers: [
        tf.layers.dense({
          units: 200,
          inputShape: [1024],
          activation: 'relu'
        }),
        tf.layers.dense({
          units: 100,
          activation: 'relu',
          kernelInitializer: 'varianceScaling',
          useBias: true
        }),
        tf.layers.dense({
          units: that.labels.length,
          kernelInitializer: 'varianceScaling',
          useBias: false,
          activation: 'softmax'
        })
      ]
    });

    // 0.0001 es el learning rate
    const optimizer = tf.train.adam(0.0001);

    this.targetModel.compile({
      optimizer: optimizer,
      loss: 'categoricalCrossentropy',
      metrics: ['accuracy']
    });
  }

  extractFeature(image) {
    image.width = IMAGE_SIZE;
    image.height = IMAGE_SIZE;
    const t_image = tf.browser.fromPixels(image);
    console.log(t_image);
    const t_activation = this.mobilenet.infer(t_image, 'conv_preds');
    t_image.dispose();
    return t_activation;
  }

  addLabeledImage(labeledImage) {

    // si la label (string) no existe en el array de labels, la añadimos
    if (this.labels.indexOf(labeledImage.label) == -1) {
      this.labels.push(labeledImage.label);
    }

    const t_activation = this.extractFeature(labeledImage.data);
    let t = t_activation.squeeze();
    this.dataset.data.push(t);
    this.dataset.dataArray.push(t.dataSync());
    // Añadimos al array de labels del dataset el índice que el corresponde
    // a la label en el array labels.
    this.dataset.labels.push(this.labels.indexOf(labeledImage.label));

    t_activation.dispose();
  }

  buildDataset() {
    this.labels = [];
    this.dataset.labels = [];
    for (let t of this.dataset.data) {
      t.dispose();
    }
    this.dataset.data = [];
    this.dataset.dataArray = [];
    console.log(tf.memory());
    for (const k of this.labeledDataService.labelsWithData.keys()) {
      for (const image of this.labeledDataService.labelsWithData.get(k)) {
        this.addLabeledImage({ label: k, data: image })
      }
    }
    console.log(this.dataset);
  }

  setTraindata(labelsWithData: Map<Label, Set<HTMLImageElement>>) {
    this.clear();

    for (let label of labelsWithData) {
      for (let data of Array.from(label["1"])) {
        this.addLabeledImage(
          {
            label: label,
            data: data
          }
        );
        //this.traindata.push({ label: label[0], text: data });
        console.log(data);
      }
    }

    /* console.log(this.traindata);
    this.brainText.addData(this.traindata); */
  }

  train(): Observable<any> {

    return Observable.create(observer => {
      setTimeout(() => {
        this.labeledDataService.state = MLState.TRAINING;
        this.buildDataset();
        this.buildModel();
        const inputs = tf.stack(this.dataset.data);
        const labels = tf.tidy(() => tf.oneHot(tf.tensor1d(this.dataset.labels, 'int32'), this.labels.length));

        function onBatchEnd(batch, logs) {
          console.log('Accuracy', logs.acc);
        }

        //Train for 5 epochs with batch size of 32.
        this.targetModel.fit(inputs, labels, {
          epochs: 20,
          batchSize: 4,
          callbacks: { onBatchEnd },
          shuffle: true
        }).then(info => {
          console.log('Final accuracy', info.history.acc);
          this.labeledDataService.state = MLState.TRAINED;
          this.targetModel.save('indexeddb://image-model')
            .then(() => {
              let ch = new BroadcastChannel('new-model');
              ch.postMessage('new model saved');
            });
          localStorage.setItem('imageLabels', JSON.stringify(this.labels));
          localStorage.setItem('imageDataset', JSON.stringify({
            labels: this.dataset.labels,
            dataArray: this.dataset.dataArray
          }));
          //this.targetModel.save('downloads://image-model');
          inputs.dispose();
          labels.dispose();
          observer.next(info);
          return info;
        });

      }, 200)
    });

  }

  async classify(image) {
    const t_activation = this.extractFeature(image);
    const prediction = await this.targetModel.predict(t_activation);


    const predictions = prediction.dataSync();
    const arr_predictions = Array.from(predictions);
    let results = [];
    for (let i = 0; i < arr_predictions.length; i++) {
      results.push([this.labels[i], arr_predictions[i]]);
    }
    results.sort((a, b) => b[1] - a[1]);
    t_activation.dispose();
    return results;
  }
}