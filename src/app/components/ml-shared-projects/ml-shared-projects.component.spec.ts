import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MlSharedProjectsComponent } from './ml-shared-projects.component';

describe('MlSharedProjectsComponent', () => {
  let component: MlSharedProjectsComponent;
  let fixture: ComponentFixture<MlSharedProjectsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MlSharedProjectsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MlSharedProjectsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
